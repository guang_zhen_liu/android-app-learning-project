package com.lgz.chapter09;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class BroadStaticActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_broad_static);
        findViewById(R.id.btn_send_shock).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        // Android8.0 之后删除了大部分静态注册，防止退出App后仍在接收广播
        // 为了让应用能够继续接收静态广播，需要给静态注册的广播指定包名
        String fullName  = "com.lgz.chapter09.receiver.ShockReceiver";
        Intent intent = new Intent("com.lgz.chapter09.shock");
        ComponentName componentName = new ComponentName(this, fullName);
        intent.setComponent(componentName);
        sendBroadcast(intent);
    }
}
package com.lgz.chapter09.receiver;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import com.lgz.chapter09.AlarmActivity;

public class AlarmReceiver extends BroadcastReceiver {

    public static final String ALARM_ACTION = "com.lgz.chapter09.alarm";

    private final Context mContext;

    public AlarmReceiver(Context context) {
        super();
        this.mContext = context;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent != null && intent.getAction().equals(ALARM_ACTION)) {
            Log.d("lll", "收到闹钟广播");
            sendAlarm();
        }
    }

    // 发送闹钟广播
    public void sendAlarm() {
        Intent intent = new Intent(ALARM_ACTION);
        // 创建一个用于广播的延迟意图
        // android12以上强烈建议使用FLAG_IMMUTABLE
        PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext, 0, intent, PendingIntent.FLAG_IMMUTABLE);
        // 从系统服务中获取闹钟管理器
        AlarmManager alarmManager = (AlarmManager)mContext.getSystemService(Context.ALARM_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //允许在空闲时发送广播，android6.0后新增的方法
            alarmManager.setAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, 1000, pendingIntent);
        } else {
            // 设置一次性闹钟，延迟若干秒后，携带延迟意图发送闹钟广播，但android6.0后，set方法在暗屏时不保证会发送广播
            // 必调用setAndAllowWhileIdle方法
            alarmManager.set(AlarmManager.RTC_WAKEUP, 1000, pendingIntent);
        }
    }
}

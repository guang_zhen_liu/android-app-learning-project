package com.lgz.chapter03;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.lgz.chapter03.util.ViewUtil;

public class EditHideActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_hide);
        EditText et_phone = findViewById(R.id.et_phone);
        EditText et_passwd = findViewById(R.id.et_passwd);

        et_phone.addTextChangedListener(new HideTextWatcher(et_phone, 11));
        et_passwd.addTextChangedListener(new HideTextWatcher(et_passwd, 6));
    }

    private class HideTextWatcher implements TextWatcher {

        private EditText mview;
        private int mMaxlength;

        public HideTextWatcher(EditText v, int maxLength) {
            this.mview = v;
            this.mMaxlength = maxLength;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            String str = editable.toString();
            if (str.length() == mMaxlength) {
                ViewUtil.hideOneInputMethod(EditHideActivity.this, mview);
            }
        }
    }
}
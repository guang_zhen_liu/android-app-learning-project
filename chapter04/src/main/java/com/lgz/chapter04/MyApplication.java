package com.lgz.chapter04;

import android.app.Application;
import android.content.res.Configuration;
import android.util.Log;

import androidx.annotation.NonNull;

import java.util.HashMap;

public class MyApplication extends Application {

    private static MyApplication mApp;
    public HashMap<String, String> infoMap = new HashMap<>();

    public static MyApplication getInstance() {
        return mApp;
    }

    // 在app启动时调用
    @Override
    public void onCreate() {
        super.onCreate();
        mApp = this;
        Log.d("lgz", "MyApplication onCreate");
    }

    // 在配置改变时调用，例如从竖屏变为横屏
    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.d("lgz", "MyApplication onConfigurationChanged");
    }
}

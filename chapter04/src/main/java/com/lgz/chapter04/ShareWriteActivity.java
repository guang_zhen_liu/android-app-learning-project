package com.lgz.chapter04;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

public class ShareWriteActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText ed_name;
    private EditText et_age;
    private EditText et_height;
    private EditText et_weight;
    private CheckBox cb_married;
    private Button btn_save;
    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_write);

        ed_name = (EditText)findViewById(R.id.ed_name);
        et_age = (EditText)findViewById(R.id.et_age);
        et_height = (EditText)findViewById(R.id.et_height);
        et_weight = (EditText)findViewById(R.id.et_weight);
        cb_married = (CheckBox)findViewById(R.id.cb_married);
        btn_save = (Button)findViewById(R.id.btn_save);

        btn_save.setOnClickListener(this);

        preferences = getSharedPreferences("name", Context.MODE_PRIVATE);

        reload();
    }

    private void reload() {
        String name = preferences.getString("name", null);
        if (name != null) {
            ed_name.setText(name);
        }

        int age = preferences.getInt("age", 0);
        if (age != 0) {
            et_age.setText(String.valueOf(age));
        }

        float height = preferences.getFloat("height", 0f);
        if (height != 0f) {
            et_height.setText(String.valueOf(height));
        }

        float weight = preferences.getFloat("weight", 0f);
        if (weight != 0f) {
            et_weight.setText(String.valueOf(weight));
        }

        boolean married = preferences.getBoolean("married", false);
        cb_married.setChecked(married);
    }

    @Override
    public void onClick(View view) {
        String name = ed_name.getText().toString();
        String age = et_age.getText().toString();
        String height = et_height.getText().toString();
        String weight = et_weight.getText().toString();

        SharedPreferences.Editor edit = preferences.edit();
        edit.putString("name", name);
        edit.putInt("age", Integer.parseInt(age));
        edit.putFloat("height", Float.parseFloat(height));
        edit.putFloat("weight", Float.parseFloat(weight));
        edit.putBoolean("married", cb_married.isChecked());
        edit.commit();
    }
}
package com.lgz.chapter04;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.FileUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.lgz.chapter04.util.FileUtil;

import java.io.File;

public class ImageWriteActivity extends AppCompatActivity implements View.OnClickListener {

    private String path;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_write);

        findViewById(R.id.btn_read).setOnClickListener(this);
        findViewById(R.id.btn_save).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_save:
                String fileName = System.currentTimeMillis() + ".jpeg";
                // 获取当前app的私有下载目录
                path = getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + File.separatorChar + fileName;
                // 从指定的资源文件中获取位图
                Bitmap b1 = BitmapFactory.decodeResource(getResources(), R.drawable.diqiu);
                FileUtil.saveImage(path, b1);
                Toast.makeText(this, "保存成功", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn_read:
//                Bitmap b2 = FileUtil.openImage(path);
//                Bitmap b2 = BitmapFactory.decodeFile(path);

//                ImageView iv_content = (ImageView)findViewById(R.id.iv_content);
//                iv_content.setImageBitmap(b2);
                ImageView iv_content = (ImageView)findViewById(R.id.iv_content);
                iv_content.setImageURI(Uri.parse(path));

                break;
        }
    }
}
package com.lgz.chapter04;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import java.util.HashMap;

public class AppWriteActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText ed_name;
    private EditText et_age;
    private EditText et_height;
    private EditText et_weight;
    private CheckBox cb_married;
    private Button btn_save;
    private MyApplication mApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_write);

        ed_name = (EditText)findViewById(R.id.ed_name);
        et_age = (EditText)findViewById(R.id.et_age);
        et_height = (EditText)findViewById(R.id.et_height);
        et_weight = (EditText)findViewById(R.id.et_weight);
        cb_married = (CheckBox)findViewById(R.id.cb_married);
        btn_save = (Button)findViewById(R.id.btn_save);

        btn_save.setOnClickListener(this);
        reload();
    }

    private void reload() {
        if (mApp == null)
            mApp = MyApplication.getInstance();
        HashMap<String, String> infoMap = mApp.infoMap;

        String name = infoMap.get("name");
        if (name == null)
            return;
        String age = infoMap.get("age");
        String height = infoMap.get("height");
        String weight = infoMap.get("weight");
        String married = infoMap.get("married");

        ed_name.setText(name);
        et_age.setText(age);
        et_height.setText(height);
        et_weight.setText(weight);
        cb_married.setChecked(Boolean.getBoolean(married));
    }

    @Override
    public void onClick(View view) {
        String name = ed_name.getText().toString();
        String age = et_age.getText().toString();
        String height = et_height.getText().toString();
        String weight = et_weight.getText().toString();

        mApp = MyApplication.getInstance();
        HashMap<String, String> infoMap = mApp.infoMap;
        infoMap.put("name", name);
        infoMap.put("age", age);
        infoMap.put("height", height);
        infoMap.put("weight", weight);
        infoMap.put("married", String.valueOf(cb_married.isChecked()));
    }
}
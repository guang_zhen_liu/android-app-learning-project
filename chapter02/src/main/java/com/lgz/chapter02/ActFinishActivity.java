package com.lgz.chapter02;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ActFinishActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_finish);
        Button bt_back = (Button) findViewById(R.id.bt_back);
        bt_back.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        finish();
    }
}
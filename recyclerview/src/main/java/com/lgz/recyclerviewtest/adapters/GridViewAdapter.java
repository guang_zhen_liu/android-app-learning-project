package com.lgz.recyclerviewtest.adapters;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.lgz.recyclerviewtest.beans.ItemBean;

import java.util.List;

public class GridViewAdapter extends RecyclerView.Adapter<GridViewAdapter.InnerHolder> {

    private final List<ItemBean> mData;

    public GridViewAdapter(List<ItemBean> data) {
        this.mData = data;
    }

    @NonNull
    @Override
    public GridViewAdapter.InnerHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View.inflate(parent.getContext(), )
        return new InnerHolder();
    }

    @Override
    public void onBindViewHolder(@NonNull GridViewAdapter.InnerHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        if (mData != null)
            return mData.size();
        return 0;
    }

    public class InnerHolder extends RecyclerView.ViewHolder {
        public InnerHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}

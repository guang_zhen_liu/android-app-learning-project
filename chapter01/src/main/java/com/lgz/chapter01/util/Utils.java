package com.lgz.chapter01.util;

import android.content.Context;

public class Utils {

    // 根据手机 dpi 将 dp 转换为 px
    public static int dip2px(Context context, float dpValue) {
        // 一个dp对应几个px
        float scale = context.getResources().getDisplayMetrics().density;
        // 四舍五入取整
        return  (int) (dpValue * scale + 0.5f);
    }
}

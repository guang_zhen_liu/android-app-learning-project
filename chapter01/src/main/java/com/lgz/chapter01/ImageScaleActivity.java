package com.lgz.chapter01;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;

public class ImageScaleActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_scale);

        ImageView imageView = findViewById(R.id.iv_scale);
        imageView.setImageResource(R.drawable.ic_launcher_background);
    }
}
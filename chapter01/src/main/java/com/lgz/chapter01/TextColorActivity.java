package com.lgz.chapter01;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.widget.TextView;

public class TextColorActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_color);

        TextView tv_code_system = findViewById(R.id.tv_code_system);
        tv_code_system.setTextColor(Color.GREEN);

        TextView tv_code_eight = findViewById(R.id.tv_code_eight);
        // 不透明的绿色，即一般的绿色
        tv_code_eight.setTextColor(0xff00ff00);

        TextView tv_code_six = findViewById(R.id.tv_code_six);
        // 透明的绿色
        tv_code_six.setTextColor(0x00ff00);

        TextView tv_code_background = findViewById(R.id.tv_code_background);
        // 设置背景颜色
//        tv_code_background.setBackgroundColor(Color.GREEN);
        // 颜色来源于资源文件
        tv_code_background.setBackgroundResource(R.color.green);
    }
}
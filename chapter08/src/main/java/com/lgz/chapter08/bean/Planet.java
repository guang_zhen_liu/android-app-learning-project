package com.lgz.chapter08.bean;

import com.lgz.chapter08.R;

import java.util.ArrayList;
import java.util.List;

public class Planet {

    public int image; //行星图标
    public String name; //行星名称
    public String desc; //行星描述

    public Planet(int image, String name, String desc) {
        this.image = image;
        this.name = name;
        this.desc = desc;
    }

    private final static String[] nameArray = {"水星", "金星", "地球", "火星", "木星", "土星"};
    private static final int[] iconArray = {
            R.drawable.diqiu, R.drawable.diqiu, R.drawable.diqiu,
            R.drawable.diqiu, R.drawable.diqiu, R.drawable.diqiu,
    };
    private static final String[] descArray = {
            "sddsgdgdsgdhthfjjfgfhfgdgdfe",
            "cdsgfgdvdfgdgdgdfgdgdfgdgd",
            "sddsgdgdsgdhthfjjfgfhfgdgdfe",
            "cdsgfgdvdfgdgdgdfgdgdfgdgd",
            "sddsgdgdsgdhthfjjfgfhfgdgdfe",
            "cdsgfgdvdfgdgdgdfgdgdfgdgd",
    };

    public static List<Planet> getDefaultList() {
        List<Planet> planetList = new ArrayList<>();
        for (int i = 0; i < iconArray.length; i++) {
            planetList.add(new Planet(iconArray[i], nameArray[i], descArray[i]));
        }
        return planetList;
    }
}

package com.lgz.chapter08.bean;

public class GoodsInfo {

    public int id;

    public String name;

    public String description;

    public float price;

    public String picPath;

    public int pic;

    private static String[] mNameArray = {
            "iPhone11", "Mate30", "小米10", "OPPO Reno3", "vivo X30", "荣耀30S"
    };

    private static String[] mDescArray = {
            "Apple iPhone11 256GB 绿色 4G全网通手机",
            "华为 HUAWEI Mate30 8GB+256GB 丹霞橙 5G全网通 全面屏手机",
            "小米 xiaomi 13 8GB+256GB 丹霞橙 5G全网通 全面屏手机",
            "OPPO OPPO Reno3 8GB+256GB 丹霞橙 5G全网通 全面屏手机",
            "vivo vivo X30 8GB+256GB 丹霞橙 5G全网通 全面屏手机",
            "荣耀 荣耀 30S 8GB+256GB 丹霞橙 5G全网通 全面屏手机",
    };
}
